package com.example.uapv1705388.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private SimpleCursorAdapter cursorAdapter;
    private WeatherDbHelper dbh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbh = new WeatherDbHelper(this);
        cursorAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                dbh.fetchAllCities(),
                new String[]{WeatherDbHelper.COLUMN_CITY_NAME,WeatherDbHelper.COLUMN_COUNTRY},
                new int[] {android.R.id.text1, android.R.id.text2},
                0);

        final ListView listview = (ListView) findViewById(R.id.CityList);
        listview.setAdapter(cursorAdapter);

        final FloatingActionButton addbutton = (FloatingActionButton) findViewById(R.id.AddCityButton);
        addbutton.setOnClickListener(new AdapterView.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent intent =  new Intent(MainActivity.this,NewCityActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                Cursor cursor = dbh.fetchAllCities();
                cursor.move(position);
                Intent intent =  new Intent(MainActivity.this,CityActivity.class);
                intent.putExtra("city",dbh.cursorToCity(cursor));
                startActivityForResult(intent,2);
            }
        });

        listview.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener(){
            @Override
            public void onCreateContextMenu(ContextMenu p1, View p2, ContextMenu.ContextMenuInfo p3)
            {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.citymenu, p1);
            }
        });

        final SwipeRefreshLayout refreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
                    @Override
                    public void onRefresh(){
                        List<City> allcities = dbh.getAllCities();
                        new UpdateWeather(new Runnable() {
                            @Override
                            public void run() {
                                notifyListView();
                            }
                        }, dbh).execute(allcities.toArray(new City[0]));


                    }
                }
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1)
        {
            if (resultCode == MainActivity.RESULT_OK) {
                City city = data.getParcelableExtra("city");
                dbh.addCity(city);
                notifyListView();
            }
        }
        if (requestCode == 2)
        {
            if (resultCode == MainActivity.RESULT_OK) {

                City city = data.getParcelableExtra("city");

                dbh.updateCity(city);
                notifyListView();
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.CityMenu:
                final Cursor allbookcursor = dbh.fetchAllCities();
                allbookcursor.move(info.position);
                dbh.deleteCity(allbookcursor);
                recreate();
                return true;
            default:
                return MainActivity.super.onContextItemSelected(item);
        }
    }

    public void notifyListView(){
       cursorAdapter.changeCursor(dbh.fetchAllCities());
       cursorAdapter.notifyDataSetChanged();

    }

}
