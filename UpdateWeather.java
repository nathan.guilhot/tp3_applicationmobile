package com.example.uapv1705388.tp3;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.xml.transform.Result;

public class UpdateWeather extends AsyncTask<City, Integer, City[]> {
    Runnable runnable;
    WeatherDbHelper dbh;

    public UpdateWeather(Runnable runnable, WeatherDbHelper dbh) {
        this.runnable = runnable;
        this.dbh = dbh;
    }

    @Override
    protected City[] doInBackground(City ... Cities) {
        int count = Cities.length;
        City[] result = new City[count];
        for (int i = 0; i < count; i++) {
            try {
                URL url = WebServiceUrl.build(Cities[i].getName(), Cities[i].getCountry());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    JSONResponseHandler jrh = new JSONResponseHandler(Cities[i]);
                    result[i] = jrh.readJsonStream(in);

                }
                finally {
                    urlConnection.disconnect();
                }

            }
            catch (IOException e) {
                e.printStackTrace();
            }

            // Escape early if cancel() is called
            if (isCancelled()) break;
        }
        return result;
    }

    @Override
    protected void onPostExecute (City[] cities)
    {
        if(dbh!=null)
        {
            for(int i = 0 ;i<cities.length;i++)
            {
                dbh.updateCity(cities[i]);
            }
        }
        if(runnable != null){
            runnable.run();
        }
    }
}
